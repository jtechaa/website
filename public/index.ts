function submitForm(id: string): Promise<Response> {
    let form: HTMLFormElement = document.getElementById(id) as HTMLFormElement;
    let formData = new FormData(form);
    if (id.includes("add")) {
        let urlEncoded = new URLSearchParams();

        formData.forEach((value, key) => {
            urlEncoded.append(key, value as string);
        });

        return fetch(
            "http://127.0.0.1:9001/add" + id.split("formadd")[1], // TODO: camel-case
            {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                method: "POST",
                mode: 'cors',
                cache: 'no-cache',
                body: urlEncoded
            } as RequestInit
        );
    } else {
        let formObject: Array<{ key: string, value: string }> = [];

        formData.forEach((value, key) => {
            formObject.push({
                key: key,
                value: value as string
            });
        });

        let queryString = "?";
        formObject.forEach((formDatum) => {
            queryString += `${formDatum.key}=${formDatum.value}&`;
        });
        queryString = queryString.slice(0, queryString.length - 1); // remove trailing &

        return fetch("http://127.0.0.1:9001/" + id.split("formget")[1] + queryString);
    }
}

async function formFlow(formId: string): Promise<void> {
    let postFormResponse: Response = await submitForm(formId);
    let txt = await postFormResponse.text();
    document.cookie = `content=${btoa(txt)}`;
    window.location.href = "generic.html";
}

document.addEventListener('DOMContentLoaded', () => {
    let forms: HTMLFormElement[] = Array.from(document.forms);
    forms.forEach((form: HTMLFormElement) => {
        form.getElementsByClassName("submit")[0].addEventListener("click", (event: Event) => {
            event.preventDefault();
            formFlow(form.id);
        });
    });
});
