function getCookie(cookieName: string): string {
    let name: string = cookieName + "=";
    let decodedCookie: string = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return atob(c.substring(name.length, c.length));
        }
    }
    throw new Error(`No cookie with the name ${cookieName}.`);
}

let text: string = getCookie("content");
let innerHtml: string = "";

if (text.charAt(0) == '{') {
    innerHtml = `<pre><code>${JSON.stringify(JSON.parse(text), null, '\t')}</code></pre>`;
} else {
    innerHtml = `<p>${text}</p>`;
}

document.getElementById("main").innerHTML = innerHtml;
